# Lab3 report

## [练习1] 给未被映射的地址映射上物理页

1 设计实现过程
根据注释中的描述
1. try to find a pte, if pte's PT(Page Table) isn't existed, then create a PT.
利用函数get_pte获得指向pte的指针
函数的第三个参数是create，为1的时候才会创建新的页表
2. if the phy addr isn't exist, then alloc a page & map the phy addr with logical addr
如果pte的物理地址不存在，那么分配一个页，使用函数pgdir_alloc_page。
同时需要考虑vma权限，传入perm参数。


2 请描述页目录项和页表中组成部分对ucore实现页替换算法的潜在用处
页目录项的内容:页表起始目录地址 & 0x0FFF）| PTE_U | PTE_W | PTE_P
页表项内容:(pa & ~0x0FFF) | PTE_P| PTE_W
两者的前缀都用于计算基址，可用于构造多级页表。
PTE_U 表示用户态软件可以读写，可以用于设置特权级，对应于保护位。
PTE_W 表示物理内存页可写，可以用于设置只读内容，对应于保护位。
PTE_P 表示物理内存页存在，对应于替换算法中的存在位。
页表项中未使用的位可以用于扩展替换算法，比如访问位、修改位等。

3 如果ucore的缺页服务例程在执行过程中访存，出现了页访问异常，请问硬件要做哪些事情
首先，保存现场并跳转到异常处理程序。处理时，需要为缺失的页分配物理地址空间，如果内存空间不够，需要根据置换算法置换。最后跳转回原来的访存代码并重新执行。


## [练习2] 补充完成基于FIFO的页面替换算法

1 设计实现过程
根据注释中的描述
1. 在vmm.c中 this pte is a swap entry
    pte在dsik中，需要直接从disk中加载页面
    1. According to the mm AND addr, try to load the content of right disk page into the memory which page managed.
    调用swap_in函数，将磁盘中需要载入的页面加载入内存
    2. According to the mm, addr AND page, setup the map of phy addr <---> logical addr
    调用page_insert函数，建立虚实地址之间的映射
    3. make the page swappable.
    调用swap_map_swappable函数
2.在swap_fifo.c中
    1. 修改_fifo_map_swappable函数，将list_entry_t加入链表末尾
    2. 修改_fifo_swap_out_victim函数，删除链表头，返回页帧的物理地址。

2 如果要在ucore上实现extended clock页替换算法，请给出你的设计方案，现有的swap_manager框架是否足以支持在ucore中实现此算法
现有的框架不支持。因为无法在访存时对访存位进行修改，并且没有内存写回的支持。
extended clock页算法需要被换出的页的特征是访存位和修改位均为0，且CLOCK指针刚好指到。可在页表项中拓展出这两个比特。当所访问的页不在内存时进行换入操作，当内存中无空闲的页且需要换入时执行换出操作。
我的设计：页表项中拓展出访存位和修改位两位，并在访存时更新。当需要换出时，依次查找CLOCK指针所指的项，如果两位均为0时换出此项。否则，按照修改规则修改这两位，并将CLOCK指针指向下一项。